Events = new Meteor.Collection('events');

UserEvents = new Meteor.Collection('userevents');

if (Meteor.isServer) {
    Events ._ensureIndex({'place.loc':'2dsphere'});
} 

SEO = new FlowRouterSEO();

SEO.setDefaults({
  title: 'EventEnix',
  description: 'Find, share and attend music events around you',  
  meta: {
    'name="keywords"': 'Events, event, music, concert, gig, jam, underground, festival, Oslo, London',
    'name="copyright"': 'EventEnix',
    'name="author"': 'EventEnix',    
    'property="og:type"': 'website',
    'property="og:site_name"': 'EventEnix',
    'property="og:title"': 'EventEnix',
    'property="og:image"': 'http://eventenix.com/img/jumbotron1.png',
    'property="og:description"': 'Find, share and attend music events around you',
    'property="fb:app_id"': '1653302234890326',
    'name="twitter:card"': 'Find, share and attend music events around you',
    'name="twitter:site"': '@EventEnix'
  }
});