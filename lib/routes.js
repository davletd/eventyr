FlowRouter.route( '/', {
	action: function() {
        GAnalytics.pageview();
    	  BlazeLayout.render( 'applicationLayout', { 
        header: 'headerTemplate',
        main: 'application',
        footer: 'footerTemplate'
      }); 
    },
  name: 'home' // Optional route name.
});

FlowRouter.route( '/myevents', {
	action: function() {
        GAnalytics.pageview('/myevents');
    	BlazeLayout.render( 'applicationLayout', { 
        header: 'headerTemplate',
        main: 'myevents',
        footer: 'footerTemplate'
      }); 
    },
  name: 'myevents' // Optional route name.
});


FlowRouter.route( '/terms', {
	action: function() {
    	BlazeLayout.render( 'applicationLayout', { 
        header: 'headerTemplate',
        main: 'termsOfService',
        footer: 'footerTemplate'
      }); 
  },
  name: 'termsOfService' // Optional route name.
});


FlowRouter.route( '/event', {    
    action: function() {
        console.log( "Okay, we're on the Event page!" );
    },
    name: 'Event' // Optional route name.
});

var events = FlowRouter.group({
  prefix: '/events'
});

// http://app.com/events
events.route( '/', {
  action: function() {
    console.log( "We're viewing a list of events." );
  }
});

// http://app.com/events/:eventId
events.route( '/:eventId', {
  action: function( params ) {
    GAnalytics.pageview("/event");
    BlazeLayout.render( 'applicationLayout', { 
        header: 'headerTemplate',
        main: 'event',
        footer: 'footerTemplate' 
      }); 
  },
  name: 'eventId'
});

// http://app.com/events/:eventId/edit
events.route( '/:eventId/edit', {
  action: function() {
    console.log( "We're editing a single document." );
  }
});