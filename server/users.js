Accounts.onCreateUser(function(options, user) {
  email = user.email;
  if (options.profile) {
    user.profile = options.profile;    
  }    
  return user;
});


Meteor.publish('userData', function() {
  return Meteor.users.find({}, {
    fields: {
      services: 1,
    }
  });
})