ServiceConfiguration.configurations.remove({
    service: 'facebook'
});
 
ServiceConfiguration.configurations.upsert(
  { service: "facebook" },
  {
    $set: {
      appId: Meteor.settings.facebookAppId,
      secret: Meteor.settings.facebookSecret,
      loginStyle: "redirect"
    }
  }
);