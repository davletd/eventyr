function Facebook(accessToken) {
    this.fb = Meteor.npmRequire ('fbgraph');
    this.accessToken = accessToken;
    this.fb.setAccessToken(this.accessToken);
    this.options = {
        timeout: 3000,
        pool: {maxSockets: Infinity},
        headers: {connection: "keep-alive"},
    }
    this.fb.setOptions(this.options);
}

Facebook.prototype.query = function(query, method) {
    var self = this;
    var method = (typeof method === 'undefined') ? 'get' : method;
    var data = Meteor.sync(function(done) {
        self.fb[method](query, function(err, res) {
            done(null, res);
        });
    });
    return data.result;
}

Facebook.prototype.getEventData = function(eventid) {
    return this.query(eventid + 
        '?fields=id,description,cover,name,start_time,end_time,place,category,ticket_uri');
}

Facebook.prototype.getMyEvents = function() {
    return this.query('me/events');
}

Facebook.prototype.rsvpAttending = function(eventid) {
    return this.query(eventid + '/attending', 'post');
}

Facebook.prototype.rsvpInterested = function(eventid) {
    return this.query(eventid + '/maybe', 'post');
}

Facebook.prototype.rsvpNotGoing = function(eventid) {
    return this.query(eventid + '/declined', 'post');
}

Facebook.prototype.getRsvpAttending = function(eventid, userId) {
    return this.query(eventid + '/attending?user=' + userId, 'get');
}

Facebook.prototype.getRsvpInterested = function(eventid, userId) {
    return this.query(eventid + '/maybe?user=' + userId, 'get');
}

Meteor.methods({
    getEventData: function(eventinput) {
        var fb = new Facebook(Meteor.user().services.facebook.accessToken);
        var eventid = eventinput.replace(/\D/g, '');
        var data = fb.getEventData(eventid);
        
        Events.upsert(
            { id: data.id },
            { $set: {
                "id": data.id,
                "description": data.description,
                "cover": data.cover,
                "name": data.name,
                "start_time": new Date(data.start_time),
                "end_time": new Date(data.end_time),
                "category": data.category,
                "ticket_uri": data.ticket_uri,
                "origin": "fb",
                "place": {
                        "name": data.place.name,
                        "loc" : {type: "Point", 
                                coordinates: [data.place.location.longitude, 
                                              data.place.location.latitude]},
                        "location": {                            
                            "city": data.place.location.city,
                            "country": data.place.location.country,
                            "latitude": data.place.location.latitude,
                            "longitude": data.place.location.longitude,
                            "street": data.place.location.street,
                            "zip": data.place.location.zip    
                        }
                    }
                }
            },
            function(err) {
                if(err) console.error(err);
            });        
    },
    
    getMyEvents: function() {
        var fb = new Facebook(Meteor.user().services.facebook.accessToken);
        var data = fb.getMyEvents();
        _.forEach(data.data, function(event) {
            UserEvents.upsert (
                { id: Meteor.user().services.facebook.id },
                { $addToSet: {
                        "userevents": event.id
                    },
                });  
        });     
    },
    
    rsvpAttending: function(eventId) {
        var fb = new Facebook(Meteor.user().services.facebook.accessToken);
        var success = fb.rsvpAttending(eventId);
        return success;
    },
    
    rsvpInterested: function(eventId) {
        var fb = new Facebook(Meteor.user().services.facebook.accessToken);
        var success = fb.rsvpInterested(eventId);
        return success;
    },
    
    rsvpNotGoing: function(eventId) {
        var fb = new Facebook(Meteor.user().services.facebook.accessToken);
        var success = fb.rsvpNotGoing(eventId);
        return success;
    },
    
    getRsvpAttending: function(eventId, userId) {
        var fb = new Facebook(Meteor.user().services.facebook.accessToken);
        var data = fb.getRsvpAttending(eventId, userId);
        return data ? EJSON.stringify(data).indexOf("attending") !== -1: false;
    },
    
    getRsvpInterested: function(eventId, userId) {
        var fb = new Facebook(Meteor.user().services.facebook.accessToken);
        var data = fb.getRsvpInterested(eventId, userId);
        return data ? EJSON.stringify(data).indexOf("unsure") !== -1: false;
    },
    
    removeAllEvents: function() {
        return Events.remove({});
    }
});

Meteor.publish('events', function() {
  return Events.find();
});

Meteor.publish('userevents', function() {
  return UserEvents.find();
});

