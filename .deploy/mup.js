module.exports = {
  servers: {
    one: {
      host: '13.74.248.38', //'52.169.239.13', //'40.113.19.107',
      username: 'azureuser',
      password: 'qAwSeD123'  //'qAwSeD123' //'qAwSeD123`'
    }
  },

  meteor: {
    name: 'eventenix',
    path: '../.',
    servers: {
      one: {}
    },
    buildOptions: {
      serverOnly: true,
      setupMongo: true
    },    
    env: {
      ROOT_URL: 'http://eventenix.com/'
    },
    dockerImage: 'abernix/meteord:base',
    //dockerImage: 'kadirahq/meteord'
    deployCheckWaitTime: 120
  },

  mongo: {
    oplog: true,
    port: 27017,
    servers: {
      one: {}
    }
  }
};