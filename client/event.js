Template.event.onCreated(function() {
    var self = this;
    var eventId = FlowRouter.getParam('eventId');
    self.autorun(function() {        
        self.subscribe('events', eventId, {
            onReady: function() {
                var event = Events.findOne({id: eventId}) || {};
                SEO.set({
                    title: self.title,
                    description: event.name,
                    meta: {
                        'property="og:description"': event.name,
                        'property="og:image"': event.cover.source,
                        'name="twitter:image"': event.cover.source
                    }
                });
            }
        });  
    });
    
  
    GoogleMaps.ready('locationMap', function(map) {
        // Add a marker to the map once it's ready
        var marker = new google.maps.Marker({
        position: map.options.center,
        map: map.instance
        });
    });  
    

});

Template.event.onRendered(function() {
    GoogleMaps.load();
});

Template.event.helpers({
    event: function() {
        var eventId = FlowRouter.getParam('eventId');
        var event = Events.findOne({id: eventId}) || {};
        return event;
    },  
  
    dateCalendar: function(date) {
        return moment(date).calendar();
    },
  
    mapOptions: function() {
        if (GoogleMaps.loaded()) {
        return {
            center: new google.maps.LatLng(this.place.location.latitude, this.place.location.longitude),
            zoom: 16
        };
        }
    },
  
    isAttending: function() {
        var eventId = FlowRouter.getParam('eventId');
        if (Meteor.user() && Meteor.user().services) {
            Meteor.call('getRsvpAttending', eventId, Meteor.user().services.facebook.id, function(err, data) {
                $('#result').text(EJSON.stringify(data, undefined, 4));
                Session.set('attending', data);
            });
        } 
        
        return Session.get('attending');
    },
  
    isInterested: function() {
        var eventId = FlowRouter.getParam('eventId');
        if (Meteor.user() && Meteor.user().services) {
            Meteor.call('getRsvpInterested', eventId, Meteor.user().services.facebook.id, function(err, data) {
                $('#result').text(EJSON.stringify(data, undefined, 4));
                Session.set('interested', data);
            });
        } 
        
        return Session.get('interested');
    }     
});

Template.event.events({
    'click #eventJoin': function(e) {
        e.preventDefault();
        if(Meteor.user())
        {
            var eventId = FlowRouter.getParam('eventId');
            Meteor.call('rsvpAttending', eventId, function(err, data) {
                $('#result').text(EJSON.stringify(data, undefined, 4));
            });
            Session.set('attending', true);
            Session.set('interested', false);
        }
        else 
        {
            $('#loginModal').modal('show');
        }   
    },
  
    'click #eventInterested': function(e) {
        e.preventDefault();
        if(Meteor.user())
        {
            var eventId = FlowRouter.getParam('eventId');
            Meteor.call('rsvpInterested', eventId, function(err, data) {
                $('#result').text(EJSON.stringify(data, undefined, 4));
            });
            Session.set('interested', true);
            Session.set('attending', false);
        }
        else 
        {
            $('#loginModal').modal('show');
        }        
    },
    
    'click #eventNotGoing': function(e) {        
        e.preventDefault();
        if(Meteor.user())
        {
            var eventId = FlowRouter.getParam('eventId');
            Meteor.call('rsvpNotGoing', eventId, function(err, data) {
                $('#result').text(EJSON.stringify(data, undefined, 4));
            });
            Session.set('interested', false);
            Session.set('attending', false);
        }
        else 
        {
            $('#loginModal').modal('show');
        }  
    }
});

