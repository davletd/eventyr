Meteor.subscribe('userData');

Template.headerTemplate.events({
    'click #js-signin': function() {
    Meteor.loginWithFacebook(
        {requestPermissions: ['email', 'public_profile', 'user_friends', 'user_events', 'rsvp_event']}, 
        function(err){
            if (err) {
                throw new Meteor.Error("Facebook login failed");
            }          
        })
    },
  
    'click #js-logout': function() {
        Meteor.logout();
    },

    'click #create-event': function() {
        if(Meteor.user())
        {
            $('#eventModal').modal('show');
        }
        else 
        {
            $('#loginModal').modal('show');
        }
    }
});

Template.eventModal.events({
    'submit #event-form': function(e) {
        e.preventDefault();
        var input = event.target.eventid.value;
        Meteor.call('getEventData', input, function(err, data) {
            $('#result').text(EJSON.stringify(data, undefined, 4));
        });
        $('#eventModal').modal('hide');
    }
});