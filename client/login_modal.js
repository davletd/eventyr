Template.loginModal.events({
    'click #loginConfirm': function(e) {
    e.preventDefault();
    Meteor.loginWithFacebook(
        {requestPermissions: ['email', 'public_profile', 'user_friends']}, 
        function(err){
            if (err) {
                throw new Meteor.Error("Facebook login failed");
            }          
        })
    $('#loginModal').modal('hide');
    },
});


