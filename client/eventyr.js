Meteor.subscribe('events');
Meteor.subscribe('userevents');

Meteor.startup(function () {
    var locale = window.navigator.userLanguage || window.navigator.language;
    moment.locale(locale);
});

Template.eventList.helpers({
    events: function () {
        var location = new ReactiveVar({ lat: 59.9, lng: 10.7 });
        if(FlowRouter.current().path === '/myevents')
        {
            Meteor.call('getMyEvents', function(err, data) {
                if (err)
                    console.log(err);
            });     
            
            var data = UserEvents.findOne(
                {id: Meteor.user().services.facebook.id }
            ).userevents;
                                   
            return Events.find(
                {$or: [{start_time: {$gte: new Date()}}, {end_time: {$gte: new Date()}}],
                id: {$in: data}},
                {sort: {start_time: 1}});
        }
        else 
        {
            return Events.find(
                {$or: [{start_time: {$gte: new Date()}}, {end_time: {$gte: new Date()}}],
                "place.loc": {
                        $near: {
                        $geometry: {
                            type: "Point",
                            coordinates: [location.curValue.lng, location.curValue.lat]
                        },
                        $maxDistance: 25000  //meters
                        }
                    }
                },
                {sort: {start_time: 1}});
        }
    },
    
    'allevents': function() {
        if(FlowRouter.current().path === '/myevents') {
            return '';
        }
        else {
            return 'active';
        }        
    },
    
    'myevents': function() {        
        if(FlowRouter.current().path === '/myevents') {
            if(!Meteor.user())
            {
                $('#loginModal').modal('show');
            }
            return 'active';
        }
        else {
            return '';
        }        
    }
});

Template.eventItem.helpers({
    dateFromNow: function(date) {          
        return moment(date).fromNow();
    },
    dateCalendar: function(date) {
        return moment(date).calendar();
    }       
});


